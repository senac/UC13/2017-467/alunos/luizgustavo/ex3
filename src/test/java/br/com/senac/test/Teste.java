/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.Principal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class Teste {

    public Teste() {
    }
   @Test 
   public void converterParaDolar(){
       Principal calc = new Principal();
       double real = 10;
       double result = calc.calcDolar(real);
       
       assertEquals(2.45,result, 0.01);
   }
   @Test
   public void converterParaEuro(){
       Principal calc = new Principal();
       double real = 100;
       double result = calc.calcEuro(real);
       
       assertEquals(20.87,result, 0.01);
   }
   @Test
   public void convereterParaDolarAust(){
       Principal calc = new Principal();
       double real = 1000;
       double result = calc.calcDolarAust(real);
       
       assertEquals(337.83,result,0.01);
   }
}
