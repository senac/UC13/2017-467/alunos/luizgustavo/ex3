/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/*Implemente um conversor de Moedas. O conversor deve converter de BRL (real) para
as seguintes moedas dólar Americano (USD), Euro (EUR), dólar Australiano. Crie um
método converter que recebera os seguintes parâmetros : valorOrigem ,
valorResultado. Lembre-se da orientação a objetos.

 */
public class Principal {
    
    private final double DOLAR = 4.09;
    private final double EURO = 4.79;
    private final double DOLAR_AUST = 2.96;
    
    
    public double calcDolar(double valorReal){
        double usd = valorReal / DOLAR;
        
        
        return usd;
    }
    
    public double calcEuro(double valorReal){
        double eur = valorReal / EURO;
        
        return eur;
    }
    
    public double calcDolarAust(double valorReal){
      double dolarAust = valorReal / DOLAR_AUST;
      return dolarAust;
    }
    
}
